export default {
    mode: 'spa',
    /*
     ** Headers of the page
     */
    head: {
        title: 'alsagone - Public portfolio',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },
    /*
     ** Customize the generated output folder
     */
    generate: {
        dir: 'public'
    },

    /*
     ** Customize the base url
     */
    router: {
        base: '/public-portfolio/'
    },
    /*
     ** Global CSS
     */
    css: [],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/vuetify',
        '@nuxt/typescript-build',
        '@nuxtjs/fontawesome'
    ],
    fontawesome: {
        icons: {
            solid: true,
            brands: true
        }
    },
    /*
     ** Nuxt.js modules
     */
    modules: [],
    /*
     ** vuetify module configuration
     ** https://github.com/nuxt-community/vuetify-module
     */
    vuetify: {
        customVariables: [],
        theme: {
            disable: true,
            dark: true
        }
    },
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {}
    }
};
